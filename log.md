# 100 Days Of Code - Log

### Jour 0: 17 juin 2018

Allez on se lance

**Today's progress**: clonage du repo, ajout de quelques ébauches de projets qui pourraient servir de base
à quelques approfondissements.

**Thoughts**: je suis tout rouillé du git ! ça ne va pas me faire de mal non plus de m'y replonger un peu

### Jour 1: 18 juin 2018

**Today's progress**: ajout d'un affichage graphique à mon crêpier, en
jouant un peu avec le module turtle.

**Thoughts**: j'étais plutôt parti pour creuser du côté de pygame, mais en
fait turtle, au dela du simple dessin géométrique style LOGO, ça permet de
faire des petites animations rapidement. Cool.

**Link to work**
[Une vidéo du résultat](https://twitter.com/vonfesse/status/1008786351372820480)

### Jour 2: 19 juin 2018

**Today's progress**: inscription au challenge Code of Kutulu de CodinGame,
et passage en ligue bois 2 à l'aide d'une solution relativement naïve.

**Thoughts**: beaucoup de stratégies possibles à explorer !

**Link to work**
[Un exemple de niveau](https://www.codingame.com/replay/318586619)

