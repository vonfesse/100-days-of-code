from turtle import *
import time
import random

#### AFFICHAGE DE CREPES AVEC TURTLE !
# (wtf ?)

def gen_crepes(n):
    cl = []
    for i in range(n):
        w = 20+i*20
        p = (-w,0), (w,0), (w,10), (-w,10)
        s = Shape("compound")
        s.addcomponent(p,"red")
        register_shape("sh"+str(i), s)
        
        t = Turtle()
        t.shape("sh"+str(i))
        t.left(90)
        t.penup()
        cl.append(t)
    return cl

    
def move_crepes(cl, pile):
    assert len(cl) == len(pile)
    
    for i in range(len(cl)):
        cc = cl[pile[i]]
        cc.goto(0,i*25)
    
    time.sleep(1)

#### CREPIER

# pile de crepes : liste
# 1er element : bas de la pile

# un crepier itératif
def crepier(l,printer):    
    # on copie la liste, pour pas modifier l'originale
    l = l[:]    
    length = len(l)
    printer(l)
    
    # on traite la pile de crepe en partant du bas (= début de la liste)
    for i in range(0,length-1):
        m = max(l[i:])
        if l[i] < m:
            idx = l.index(m)

            # si la plus grande n'est pas en haut
            # de la pile -> paf un coup de spatule
            if(idx < length-1):
                ltmp = l[idx:]
                ltmp.reverse()
                l[idx:] = ltmp
                printer(l)
            
            # et hop, on retourne pour se retrouver
            # avec une nouvelle crepe en bonne position
            ltmp = l[i:]
            ltmp.reverse()
            l[i:] = ltmp
            printer(l)
    return l


#### AIRE DE LANCEMENT
# Essayons de faire marcher tout ça

def creperie(n):
    
    # ordre des crepes
    #l = [0,3,5,2,8,1,6,7,4,9,11,10] # à générer aléatoirement ?
    l = random.sample(range(n),n)

    delay(0)
    
    # liste de crepes
    cl = gen_crepes(len(l))
    move_crepes(cl,l)
    
    delay(10)
    
    # notre printer
    def my_printer(l):
        move_crepes(cl,l)
        
    # et on balance tout ça hop hop hop
    crepier(l,my_printer)
    
    exitonclick()