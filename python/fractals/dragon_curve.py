from operator import xor
from turtle import *
from collections import deque

# fractale

def pouet(n):
    s = []    
    for i in range(n):
        s.append(1)
        dpi = 2 ** i
        for j in range(1,dpi):
            #print(j)
            s.append(s[dpi-j-1] ^ 1)
    return s
    
def pouet_old(n):
    s = [1]
    
    for i in range(n-1):
        ns = list(map(lambda x:x^1,s))
        ns.reverse()
        s = s + [1] + ns
        
    return s

def pouet_lg(n):
    l1 = deque()
    l2 = deque()
    l3 = deque()
    l1.append(1)
    
    while n>0:
        for x in l1:
            l2.append(x)
            l3.appendleft(x^1)
        l2.append(1)
        l1 = l2 + l3
        l2.clear()
        l3.clear()
        n = n -1
        
    return l1

def is2p(n):
    return bool(n and not (n&(n-1)))


def pouetalone(x):
    if is2p(x):
        return 1
    else:
        n = 2**(x.bit_length())
        return (pouetalone(n-x) ^ 1)

a = pouet_lg(17)
ht()
speed(0)
tracer(100,0)

penup()
setpos(-200,-200)
pendown()


for dir in a:
    forward(1)
    if dir == 1:
        left(90)
    else:
        right(90)
    
mainloop()