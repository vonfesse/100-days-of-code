from turtle import *

def vonkoch(n,l):
    p = 0
    if n < 1:
        fd(l)
        p = l
    else:
        nl = l/3
        p+= vonkoch(n-1,nl)
        left(60)
        p+= vonkoch(n-1,nl)
        right(120)
        p+= vonkoch(n-1,nl)
        left(60)
        p+= vonkoch(n-1,nl)
    return p

def triangle_vk(n,l):
    for _ in range(3):
        vonkoch(n,l)
        right(120)

def mucho_tvk(n,l):
    for i in range(n):
        triangle_vk(i,l)
        up()
        fd(l+10)
        down()

def laucher(f,n,l):
    speed(100)
    up()
    goto(-400,200)
    down()
    print(f(n,l))

    mainloop()
    
    
# 4 , 162